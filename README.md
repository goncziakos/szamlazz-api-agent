# Szamlazz.hu PhpApiAgent
v2.8.2 with small upgrades

Original source: [PHPApiAgent.zip](https://www.szamlazz.hu/szamla/docs/PHPApiAgent.zip)

## Upgrades
- Add base path optional argument 

## Symfony service implementation example
```yaml
    services:
    
        SzamlaAgent\SzamlaAgentAPI:
            factory:   ['SzamlaAgent\SzamlaAgentAPI', create]
            arguments:
                - '%szamlazz_api_key%' # apiKey Számla Agent kulcs
                - true # downloadPdf  szeretnénk-e letölteni a bizonylatot PDF formátumban
                - '%szamlazz_log_level%' # logLevel naplózási szint
                - 1 # responseType válasz típusa (szöveges vagy XML)
                - '' # aggregator   webáruházat futtató motor neve
                - "%kernel.project_dir%/var/szamlazz" # basePath mentett log, pdf, xml fájlok alapértelmezett útvonala
        calls:
            - [setCallMethod, [2]] # 2: CALL_METHOD_CURL   - CURL
```
## Documentation
[Szamla_Agent_PHP_API_v2.8.pdf](docs/Szamla_Agent_PHP_API_v2.8.pdf)
