<?php
    /**
     * Ez a példa megmutatja, hogy hogyan kérdezzük le egy adózó adatait (név, cím) törzsszám alapján.
     */
    require __DIR__ . '/autoload.php';

    use \SzamlaAgent\SzamlaAgentAPI;

    try {
        // Számla Agent létrehozása alapértelmezett adatokkal
        $agent = SzamlaAgentAPI::create('agentApiKey');
        // Adózó adatainak lekérdezése törzsszám (adószám első 8 számjegye) alapján
        $result = $agent->getTaxPayer('12345678');

        // Agent válasz sikerességének ellenőrzése
        if ($result->isSuccess()) {
            // A válasz adatai további feldolgozáshoz
            $data = $result->getData();
            // Adózó adatainak megjelenítése
            echo $result->getTaxPayerStr();
        }
    } catch (\Exception $e) {
        $agent->logError($e->getMessage());
    }